import "./App.css";
import Ex_ShoeShop from "./ShoeList/Ex_ShoeShop";

function App() {
  return (
    <div className="App">
      <Ex_ShoeShop />
    </div>
  );
}

export default App;
